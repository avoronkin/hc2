-- loading the module
local api = require('api')

box.cfg{}

function tnt_rest(req)
    return api.hello()
end

-- function foo(req, ...)
--     local status = 200
--     local headers = {
--       ["X-Tarantool"] = "FROM_TNT",
--     }
--     local body = 'It works!'
--     return status, headers, body
-- end
